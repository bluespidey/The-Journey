// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.

#include "TheJourney.h"
#include "TheJourneyGameMode.h"
#include "TheJourneyHUD.h"
#include "TheJourneyCharacter.h"

ATheJourneyGameMode::ATheJourneyGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ATheJourneyHUD::StaticClass();
}
