// Copyright 1998-2016 Epic Games, Inc. All Rights Reserved.
#pragma once
#include "GameFramework/GameMode.h"
#include "TheJourneyGameMode.generated.h"

UCLASS(minimalapi)
class ATheJourneyGameMode : public AGameMode
{
	GENERATED_BODY()

public:
	ATheJourneyGameMode();
};



